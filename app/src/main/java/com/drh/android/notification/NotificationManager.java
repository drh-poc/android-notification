package com.drh.android.notification;

import android.content.Context;
import android.view.Gravity;
import android.widget.PopupWindow;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

public class NotificationManager {

  private static NotificationManager instance;

  private final List<PopupWindow> windows;
  private Context currentContext;
  private int height;

  private NotificationManager() {
    windows = new ArrayList<>();
  }

  public static NotificationManager instance() {
    return instance != null ? instance : (instance = new NotificationManager());
  }

  public void dispatch(Context context, PopupWindow window, View parent) {
    if (currentContext == null) {
      currentContext = context;
    }

    window.getContentView().measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);

    windows.removeIf(w -> !w.isShowing());
    windows.add(window);
    int height = getHeight();
    window.showAtLocation(parent, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, height);

    System.out.println("HERE HEIGHT ADD: " + height);
  }

  private int getHeight() {
    return windows.stream().mapToInt(w -> w.getContentView().getMeasuredHeight()).sum();
  }

  public void dismiss(PopupWindow popupWindow) {
//    int index = windows.indexOf(popupWindow);
//    int height = 0;
//    for (int i = 0; i < index; i++) {
//      PopupWindow window = windows.get(i);
////      window.getContentView().measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
//      height += window.getContentView().getHeight();
//    }
//
//    for (int i = index + 1; i < windows.size(); i++) {
//      PopupWindow window = windows.get(i);
////      window.getContentView().measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
//      int newHeight = window.getContentView().getHeight();
//      window.update(0, height, -1, -1);
//      height += newHeight;
//    }

//    windows.remove(index);
    windows.remove(popupWindow);
    popupWindow.dismiss();
  }

  private void redraw() {

  }

  private void clearPopups() {
    System.out.println("HERE CLEAR");
    windows.forEach(PopupWindow::dismiss);
    windows.clear();
//    height = 0;
    //TODO: impl
  }
}
