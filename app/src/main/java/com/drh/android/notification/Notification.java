package com.drh.android.notification;

import android.app.Activity;
import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Looper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.StringRes;

import java.util.Objects;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;

public class Notification extends LinearLayout {

  public static final int TYPE_NEUTRAL = 0;
  public static final int TYPE_SUCCESS = 1;
  public static final int TYPE_WARNING = 2;
  public static final int TYPE_FAILURE = 3;

  public static final int DURATION_INFINITE = -1;
  public static final int DURATION_SHORT = 2_000;
  public static final int DURATION_LONG = 5_000;

  private final int type;
  private final String message;
  private final boolean closable;
  private final int duration;

  private Notification(Activity context, int type, String message, boolean closable, int duration) {
    super(context);
    this.type = type;
    this.message = message;
    this.closable = closable;
    this.duration = duration;

  }

  public static NotificationBuilder from(Activity context) {
    return new NotificationBuilder(Objects.requireNonNull(context, "context"));
  }

  public void show() {
    LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    LinearLayout notificationRoot = (LinearLayout) layoutInflater.inflate(R.layout.notification_layout, null, false);
    notificationRoot.setVerticalGravity(Gravity.BOTTOM);
//    LinearLayout notificationRoot = findViewById(R.id.notification_root);

    Drawable drawable = notificationRoot.getBackground();//ContextCompat.getDrawable(getContext(), R.drawable.notification_shape);
    drawable.setColorFilter(new PorterDuffColorFilter(getTypeColor(type), PorterDuff.Mode.SRC_OVER));
    notificationRoot.setBackground(drawable);

    TextView tvMessage = notificationRoot.findViewById(R.id.tvMessage);
    tvMessage.setText(message);

    ImageView ivClose = notificationRoot.findViewById(R.id.ivClose);

//    PopupWindow popupWindow = new PopupWindow(
//      this,
//      LayoutParams.MATCH_PARENT,
//      LinearLayout.LayoutParams.WRAP_CONTENT,
//      false);
//
//    popupWindow.setTouchable(true);

//    popupWindow.setAnimationStyle(R.style.notification_anim);
    ((ViewGroup)((ViewGroup) ((Activity)this.getContext()).findViewById(android.R.id.content)).getChildAt(0)).addView(notificationRoot);

    if (closable) {
//      ivClose.setOnClickListener(v -> NotificationManager.instance().dismiss(popupWindow));
    } else {
      ivClose.setVisibility(INVISIBLE);
    }

//    popupWindow.getContentView().measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
//    NotificationManager.instance().dispatch(getContext(), popupWindow, this);
//    popupWindow.showAtLocation(this, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);

    //update pos
//    popupWindow.update(0, 400, -1, -1);

//    if (duration != DURATION_INFINITE) {
//      new Handler(Looper.getMainLooper()).postDelayed(() -> {
//        if (popupWindow.isShowing()) {
//          NotificationManager.instance().dismiss(popupWindow);
////          popupWindow.dismiss();
//        }
//      }, duration);
//    }
  }

  private int getTypeColor(int type) {
    switch (type) {
      case TYPE_SUCCESS:
        return getContext().getColor(R.color.success);
      case TYPE_WARNING:
        return getContext().getColor(R.color.warning);
      case TYPE_FAILURE:
        return getContext().getColor(R.color.error);
      default:
        return getContext().getColor(R.color.info);
    }
  }

  @RequiredArgsConstructor(access = AccessLevel.PRIVATE)
  public static class NotificationBuilder {
    private final Activity context;
    private int type = 0;
    private String message;
    private boolean closeable = true;
    private int duration = DURATION_SHORT;

    public Notification build() {
      return new Notification(context, type, message, closeable, duration);
    }

    public void show() {
      build().show();
    }

    public NotificationBuilder type(int type) {
      this.type = type;
      return this;
    }

    public NotificationBuilder message(String message) {
      this.message = Objects.requireNonNull(message, "message");
      return this;
    }

    public NotificationBuilder message(@StringRes int stringId) {
      this.message = context.getString(stringId);
      return this;
    }

    public NotificationBuilder message(@StringRes int stringId, Object... args) {
      this.message = context.getString(stringId, args);
      return this;
    }

    public NotificationBuilder closeable(boolean closeable) {
      this.closeable = closeable;
      return this;
    }

    public NotificationBuilder duration(int duration) throws IllegalArgumentException {
      if (duration <= 0 && duration != DURATION_INFINITE) {
        throw new IllegalArgumentException("Invalid duration, must be a positive integer (or -1 for infinite)");
      }
      this.duration = duration;
      return this;
    }
  }
}
