package com.drh.android.notification;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);


    findViewById(R.id.btnError).setOnClickListener(v -> Notification.from(this)
      .type(Notification.TYPE_FAILURE)
      .duration(Notification.DURATION_INFINITE)
      .closeable(true)
      .message("This is a failure message")
      .show());

    findViewById(R.id.btnWarn).setOnClickListener(v -> Notification.from(this)
      .type(Notification.TYPE_WARNING)
      .message("This is a warning message")
      .show());

    findViewById(R.id.btnSuccess).setOnClickListener(v -> Notification.from(this)
      .type(Notification.TYPE_SUCCESS)
      .message("This is a success message")
      .show());

    findViewById(R.id.btnNeutral).setOnClickListener(v -> Notification.from(this)
      .type(Notification.TYPE_NEUTRAL)
      .message("This is an information message This is an information message2 This is an information message3")
      .show());
  }
}